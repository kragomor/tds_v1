// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_v1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_v1, "TDS_v1" );

DEFINE_LOG_CATEGORY(LogTDS_v1)
 